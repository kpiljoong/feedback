var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var port = 80;

var redis = require('redis');
var rclient = redis.createClient();


var feedbackKey = "feedback";

app.set('views', './views');
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

app.get('/', function(req, res) {
    res.render('index', {});
});

app.post('/', function(req, res) {
    try {
        var data = {
            'email': req.body.email,
            'feedback': req.body.feedback
        };
        rclient.lpush(feedbackKey, JSON.stringify(data), function(err) {
            res.send('감사합니다!!!');
        });
    } catch (e) {
        console.log('e', e);
        res.send('등록 실패했습니다 ㅜㅜ 다시 시도 해 주세요!!!');
    }
});


var server = app.listen(port, function() {
    var host = server.address().address;
    var port = server.address().port;
    console.log('==== %s:%s', host, port);
});
